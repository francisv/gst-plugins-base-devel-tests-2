/* GStreamer
 * Copyright (C) <1999> Erik Walthinsen <omega@cse.ogi.edu>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */
#include <gst/gst.h>
#include <string.h>
#include <gst/playback/gstplay-enum.h>

GST_DEBUG_CATEGORY_STATIC (my_category);
#define GST_CAT_DEFAULT my_category

static GstAutoplugSelectResult
autoplug_select_cb (GstElement * decodebin, GstPad * pad, GstCaps * caps,
    GstElementFactory * factory, gpointer foo)
{
  GST_DEBUG_OBJECT (decodebin, "TRY");
  return GST_AUTOPLUG_SELECT_TRY;
}

static void
warning_cb (GstBus * bus, GstMessage * msg, gpointer foo)
{
  GError *err = NULL;
  gchar *dbg = NULL;

  gst_message_parse_warning (msg, &err, &dbg);

  g_printerr ("WARNING: %s (%s)\n", err->message, (dbg) ? dbg : "no details");

  g_error_free (err);
  g_free (dbg);
}

static void
error_cb (GstBus * bus, GstMessage * msg, GMainLoop * main_loop)
{
  GError *err = NULL;
  gchar *dbg = NULL;

  gst_message_parse_error (msg, &err, &dbg);

  g_printerr ("ERROR: %s (%s)\n", err->message, (dbg) ? dbg : "no details");

  g_main_loop_quit (main_loop);

  g_error_free (err);
  g_free (dbg);
}

static void
eos_cb (GstBus * bus, GstMessage * msg, GMainLoop * main_loop)
{
  g_print ("EOS\n");
  g_main_loop_quit (main_loop);
}

static void
state_cb (GstBus * bus, GstMessage * msg, GstElement * pipeline)
{
  if (msg->src == GST_OBJECT (pipeline)) {
    GstState old_state, new_state, pending_state;

    gst_message_parse_state_changed (msg, &old_state, &new_state,
        &pending_state);
    if (new_state == GST_STATE_PLAYING) {
      g_print ("Decoding ...\n");
    }
  }
}

static gboolean
print_foreach (GQuark field_id, const GValue * value, GstStructure * st)
{
  gchar *str = gst_value_serialize (value);

  /* const gchar *fname; */

  GST_INFO ("Media property: *%s* with type *%s* and value *%s*",
      g_quark_to_string (field_id), G_VALUE_TYPE_NAME (value), str);

  return TRUE;
}

static void
print_caps (GstCaps * caps)
{
  GstStructure *st;
  guint i;

  for (i = gst_caps_get_size (caps); i; i--) {
    st = gst_caps_get_structure (caps, i - 1);

    GST_INFO ("Media type *%s* with *%d* properties",
        gst_structure_get_name (st), gst_structure_n_fields (st));

    while (!gst_structure_foreach (st,
            (GstStructureForeachFunc) print_foreach, st));
  }
}

static GstCaps *
get_pad_caps (GstPad * pad)
{
  GstCaps *caps;

  /* first check the pad caps, if this is set, we are positively sure it is
   * fixed and exactly what the element will produce. */
  caps = gst_pad_get_current_caps (pad);

  /* then use the getcaps function if we don't have caps. These caps might not
   * be fixed in some cases, in which case analyze_new_pad will set up a
   * notify::caps signal to continue autoplugging. */
  if (caps == NULL)
    caps = gst_pad_query_caps (pad, NULL);

  return caps;
}

static void
pad_added_cb (GstElement * decodebin, GstPad * pad, GstElement * pipeline)
{
  GstPadLinkReturn ret;
  GstElement *fakesink;
  GstPad *fakesink_pad;
  GstCaps *caps;

  fakesink = gst_element_factory_make ("fakesink", NULL);
  fakesink_pad = gst_element_get_static_pad (fakesink, "sink");

  gst_bin_add (GST_BIN (pipeline), fakesink);

  /* this doesn't really seem right, but it makes things work for me */
  gst_element_set_state (fakesink, GST_STATE_PLAYING);

  caps = get_pad_caps (pad);

  GST_INFO_OBJECT (decodebin, "Pad %s:%s caps:%" GST_PTR_FORMAT,
      GST_DEBUG_PAD_NAME (pad), caps);

  print_caps (gst_pad_get_current_caps (pad));


  ret = gst_pad_link (pad, fakesink_pad);
  if (!GST_PAD_LINK_SUCCESSFUL (ret)) {
    GST_INFO ("Failed to link %s:%s to %s:%s (ret = %d)\n",
        GST_DEBUG_PAD_NAME (pad), GST_DEBUG_PAD_NAME (fakesink_pad), ret);
  } else {
    GST_INFO ("Linked %s:%s to %s:%s\n", GST_DEBUG_PAD_NAME (pad),
        GST_DEBUG_PAD_NAME (fakesink_pad));
  }

  gst_object_unref (fakesink_pad);
}

gint
main (gint argc, gchar * argv[])
{
  GstElement *decoder;
  GstElement *source;
  GstElement *pipeline;
  GstStateChangeReturn res;
  GMainLoop *loop;
  GstBus *bus;

  GST_DEBUG_CATEGORY_INIT (my_category, "decodetest", 0,
      "This is the debug category for my code.");

  gst_init (&argc, &argv);

  if (argc != 2) {
    g_printerr ("Decode file from start to end.\n");
    g_printerr ("Usage: %s URI\n\n", argv[0]);
    return 1;
  }

  loop = g_main_loop_new (NULL, TRUE);

  pipeline = gst_pipeline_new ("pipeline");

  bus = gst_pipeline_get_bus (GST_PIPELINE (pipeline));
  gst_bus_add_signal_watch (bus);

  g_signal_connect (bus, "message::eos", G_CALLBACK (eos_cb), loop);
  g_signal_connect (bus, "message::error", G_CALLBACK (error_cb), loop);
  g_signal_connect (bus, "message::warning", G_CALLBACK (warning_cb), NULL);
  g_signal_connect (bus, "message::state-changed", G_CALLBACK (state_cb),
      pipeline);

  source = gst_element_factory_make ("giosrc", "source");
  g_assert (source);

  if (argv[1] && strstr (argv[1], "://") != NULL) {
    g_object_set (G_OBJECT (source), "location", argv[1], NULL);
  } else if (argv[1]) {
    gchar *uri = g_strdup_printf ("file://%s", argv[1]);

    g_object_set (G_OBJECT (source), "location", uri, NULL);
    g_free (uri);
  }

  decoder = gst_element_factory_make ("decodebin", "decoder");
  g_assert (decoder);

  g_signal_connect (decoder, "autoplug-select", G_CALLBACK (autoplug_select_cb),
      NULL);


  gst_bin_add (GST_BIN (pipeline), source);
  gst_bin_add (GST_BIN (pipeline), decoder);

  gst_element_link_pads (source, "src", decoder, "sink");

  g_signal_connect (decoder, "pad-added", G_CALLBACK (pad_added_cb), pipeline);

  res = gst_element_set_state (pipeline, GST_STATE_PLAYING);
  if (res == GST_STATE_CHANGE_FAILURE) {
    g_print ("could not play\n");
    return -1;
  }

  g_main_loop_run (loop);

  /* tidy up */
  gst_element_set_state (pipeline, GST_STATE_NULL);
  gst_object_unref (pipeline);
  gst_object_unref (bus);

  return 0;
}
